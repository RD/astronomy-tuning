import math


def get_attribute(attribute):
    if attribute == "CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR":
        return 7
    if attribute == "CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR":
        return 1
    if attribute == "CU_DEVICE_ATTRIBUTE_WARP_SIZE":
        return 32
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR":
        return 1024
    if attribute == "CU_FUNC_ATTRIBUTE_NUM_REGS":
        return 255
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK":
        return 64 * 1024
    if attribute == "CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES":
        return 0
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK":
        return 96 * 1024
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X":
        return 1024
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y":
        return 1024
    if attribute == "CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z":
        return 64

    raise Exception("Invallid attribute: ", attribute)


def get_nr_blocks_per_multi_proc(block_dims, dyn_shared_mem_bytes):
    compute_cap_major = get_attribute("CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR")
    warp_size = get_attribute("CU_DEVICE_ATTRIBUTE_WARP_SIZE")
    factor = 0

    # #blocks regardless of kernel
    if compute_cap_major == 1 or compute_cap_major == 2:
        max_blocks_per_multi_proc = 8
    elif compute_cap_major == 3:
        max_blocks_per_multi_proc = 16
    elif compute_cap_major == 5:
        max_blocks_per_multi_proc = 32
    else:
        max_blocks_per_multi_proc = 32  # guess; unknown for future hardware
    factor = max_blocks_per_multi_proc

    # take block size into account
    nr_threads_per_block = block_dims[0] * block_dims[1] * block_dims[2]
    nr_threads_per_block = (nr_threads_per_block + warp_size - 1) & ~(
        warp_size - 1
    )  # assumes warpSize is a pow of 2
    max_threads_per_mp = get_attribute(
        "CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR"
    )
    threads_factor = max_threads_per_mp // nr_threads_per_block
    if threads_factor < factor:
        factor = threads_factor

    # number of registers
    nr_regs_per_thread = get_attribute("CU_FUNC_ATTRIBUTE_NUM_REGS")
    if nr_regs_per_thread > 0:
        # Mocked logic for compute_cap_major == 1
        if compute_cap_major == 1:
            compute_cap_minor = get_attribute(
                "CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR"
            )
            nr_regs_per_block = nr_regs_per_thread * nr_threads_per_block
            regs_granularity = 256 if compute_cap_minor <= 1 else 512
            nr_regs_per_block = (nr_regs_per_block + regs_granularity - 1) & ~(
                regs_granularity - 1
            )  # assumes regs_granularity is a pow of 2
        else:
            nr_regs_per_warp = nr_regs_per_thread * warp_size
            if compute_cap_major == 2:
                regs_granularity = 64
            elif compute_cap_major == 3 or compute_cap_major == 5:
                regs_granularity = 256
            else:
                regs_granularity = 256  # guess; unknown for future hardware
            nr_regs_per_warp = (nr_regs_per_warp + regs_granularity - 1) & ~(
                regs_granularity - 1
            )  # assumes regs_granularity is a pow of 2
            nr_warps_per_block = nr_threads_per_block // warp_size
            nr_regs_per_block = nr_regs_per_warp * nr_warps_per_block
        dev_nr_regs = get_attribute("CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK")
        regs_factor = dev_nr_regs // nr_regs_per_block
        if regs_factor < factor:
            factor = regs_factor

    # shared memory size
    sh_mem = get_attribute("CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES") + dyn_shared_mem_bytes
    if sh_mem > 0:
        if compute_cap_major == 1:
            sh_mem_granularity = 512
        elif compute_cap_major == 2:
            sh_mem_granularity = 128
        elif compute_cap_major == 3 or compute_cap_major == 5:
            sh_mem_granularity = 256
        else:
            sh_mem_granularity = 256  # guess; unknown for future hardware
        sh_mem = (sh_mem + sh_mem_granularity - 1) & ~(
            sh_mem_granularity - 1
        )  # assumes sh_mem_granularity is a pow of 2
        dev_sh_mem = get_attribute("CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK")
        sh_mem_factor = dev_sh_mem // sh_mem
        if sh_mem_factor < factor:
            factor = sh_mem_factor

    return factor


def predict_multi_proc_occupancy(its_block_dims, dyn_shared_mem_bytes=0):
    def ceil_div(a, b):
        return -(-a // b)

    warp_size = get_attribute("CU_DEVICE_ATTRIBUTE_WARP_SIZE")
    nr_threads_per_block = its_block_dims[0] * its_block_dims[1] * its_block_dims[2]
    nr_warps_per_block = ceil_div(nr_threads_per_block, warp_size)
    nr_blocks_per_mp = get_nr_blocks_per_multi_proc(
        its_block_dims, dyn_shared_mem_bytes
    )
    nr_warps = nr_blocks_per_mp * nr_warps_per_block

    max_threads_per_mp = get_attribute(
        "CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR"
    )
    max_nr_warps_per_mp = max_threads_per_mp // warp_size

    return nr_warps / max_nr_warps_per_mp


def compute_problem_size(
    params_nr_channels,
    params_nr_tabs,
    params_nr_samples_per_channel,
    params_time_integration_factor,
):
    assert type(params_nr_channels) == int
    assert type(params_nr_tabs) == int
    assert type(params_nr_samples_per_channel) == int
    assert type(params_time_integration_factor) == int

    class ExecConfig:
        def __init__(self, gr=(1, 1, 1), bl=(1, 1, 1), dyn_sh_mem=0):
            self.grid = gr
            self.block = bl
            self.dyn_shared_mem_size = dyn_sh_mem

    class CoherentStokesExecConfig(ExecConfig):
        def __init__(self, gr=(1, 1, 1), bl=(1, 1, 1), dyn_sh_mem=0):
            super().__init__(gr, bl, dyn_sh_mem)
            self.nr_time_parallel_threads = 0

    def smallest_factor_of(n):
        if n % 2 == 0:
            return 2

        sqrtn = int(math.sqrt(n + 1))  # +1: avoid e.g. sqrt(25)=4.999...
        for i in range(3, sqrtn + 1, 2):
            if n % i == 0:
                return i

        return n

    def set_enqueue_work_sizes(grid, block):
        pass

    def get_max_block_dims():
        return (
            get_attribute("CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X"),
            get_attribute("CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y"),
            get_attribute("CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z"),
        )

    def gcd(a, b):
        return math.gcd(a, b)

    default_nr_channels_per_block = 16
    default_nr_tabs_per_block = 16

    min_nr_channels_per_block = min(params_nr_channels, default_nr_channels_per_block)
    min_time_parallel_threads = 1
    min_nr_tabs_per_block = min(params_nr_tabs, default_nr_tabs_per_block)

    max_local_work_size = get_max_block_dims()
    max_nr_channels_per_block = min(params_nr_channels, max_local_work_size[0])
    max_time_parallel_threads = gcd(
        params_nr_samples_per_channel // params_time_integration_factor,
        max_local_work_size[1],
    )
    max_nr_tabs_per_block = min(params_nr_tabs, max_local_work_size[2])

    configs = []
    for nr_tabs_per_block in range(
        min_nr_tabs_per_block, max_nr_tabs_per_block + 1, default_nr_tabs_per_block
    ):
        nr_time_parallel_threads_per_block = min_time_parallel_threads
        while True:
            nr_time_parallel_threads_per_block *= smallest_factor_of(
                max_time_parallel_threads // nr_time_parallel_threads_per_block
            )
            for nr_channels_per_block in range(
                min_nr_channels_per_block,
                max_nr_channels_per_block + 1,
                default_nr_channels_per_block,
            ):
                nr_channel_threads = (
                    params_nr_channels // nr_channels_per_block * nr_channels_per_block
                )
                nr_time_parallel_threads = (
                    max_time_parallel_threads
                    // nr_time_parallel_threads_per_block
                    * nr_time_parallel_threads_per_block
                )
                nr_tabs_threads = (
                    params_nr_tabs // nr_tabs_per_block * nr_tabs_per_block
                )

                block_x = nr_channels_per_block
                block_y = nr_time_parallel_threads_per_block
                block_z = nr_tabs_per_block
                grid = (
                    nr_channel_threads // block_x,
                    nr_time_parallel_threads // block_y,
                    nr_tabs_threads // block_z,
                )
                block = (block_x, block_y, block_z)

                err_msgs = set_enqueue_work_sizes(grid, block)
                if not err_msgs:
                    ec = CoherentStokesExecConfig()
                    ec.grid = grid
                    ec.block = block
                    ec.nr_time_parallel_threads = nr_time_parallel_threads
                    configs.append(ec)
                else:
                    print("Skipping unsupported CoherentStokes exec config:", err_msgs)

            if nr_time_parallel_threads_per_block == max_time_parallel_threads:
                break

    assert configs

    configs_min_block_size = []
    min_threads_per_block = 64
    warp_size = get_attribute("CU_DEVICE_ATTRIBUTE_WARP_SIZE")
    while min_threads_per_block >= warp_size:
        for it in configs:
            if it.block[0] * it.block[1] * it.block[2] >= min_threads_per_block:
                configs_min_block_size.append(it)

        if configs_min_block_size:
            break
        min_threads_per_block //= 2

    if not configs_min_block_size:
        configs_min_block_size = configs

    configs_max_occupancy = []
    max_occupancy = 0.0
    for it in configs_min_block_size:
        set_enqueue_work_sizes(it.grid, it.block)
        occupancy = predict_multi_proc_occupancy(
            it.block,
        )
        if math.isclose(occupancy, max_occupancy, rel_tol=0.05):
            configs_max_occupancy.append(it)
        elif occupancy > max_occupancy:
            max_occupancy = occupancy
            configs_max_occupancy = [it]

    selected_config = None
    min_block_size_seen = (
        max_local_work_size[0] * max_local_work_size[1] * max_local_work_size[2] + 1
    )
    for it in configs_max_occupancy:
        block_size = it.block[0] * it.block[1] * it.block[2]
        if block_size < min_block_size_seen:
            min_block_size_seen = block_size
            selected_config = it

    return selected_config
