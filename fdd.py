#!/usr/bin/env python
import kernel_tuner
import numpy as np

from common import *


def tune(args):
    kernel_file = "dedispersion/fdd.cu"
    kernel_path = f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}"
    with open(kernel_path, 'r') as f:
        kernel_string = f.read()

    filename = f"fdd_{args.name}"
    if (args.suffix):
        filename = f"{filename}_{args.suffix}"

    # setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [32, 64]
    tune_params["NFREQ_BATCH_GRID"] = [64,  128]
    tune_params["NDM_BATCH_GRID"] = [4]
    tune_params["NFREQ_BATCH_BLOCK"] = tune_params["block_size_x"]
    tune_params["NCHAN_BATCH_THREAD"] = [8]
    tune_params["USE_SHARED_MEMORY"] = [0, 1]
    tune_params["USE_EXTRAPOLATE"] = [0, 1]

    # kernel arguments
    nchans = 1024
    sampletime_base = 250.0E-6  # Base is 250 microsecond time samples
    downsamp = 1.0
    t_obs = 30.0  # Observation duration in seconds
    dt = np.float32(downsamp * sampletime_base)  # s (0.25 ms sampling)
    nsamps = int(t_obs / dt)
    f0 = 1581.0  # MHz (highest channel)
    bw = 100.0  # MHz
    df = -1.0*bw/nchans  # MHz (This must be negative!)
    dm_start = 2.0   # pc cm^-3
    dm_end = 100.0  # pc cm^-3
    pulse_width = 4.0   # ms
    dm_tol = 1.25
    nfreqs = int(nsamps/2 + 1)  # number of spin frequencies

    nsamps_fft = roundUp(nsamps + 1, 16384)
    nsamps_padded = roundUp(nsamps_fft + 1, 1024)

    # Initialize DM list
    def generate_dm_list(dm_start, dm_end, dt, ti, tol):
        dt *= 1e6
        f = (f0 + ((nchans/2) - 0.5) * df) * 1e-3
        tol2 = tol*tol
        a = 8.3 * df / (f*f*f)
        a2 = a*a
        b2 = a2 * (nchans*nchans / 16.0)
        c = (dt*dt + ti*ti) * (tol2 - 1.0)

        dm_list = [dm_start]

        while (dm_list[-1] < dm_end):
            prev = dm_list[-1]
            prev2 = prev*prev
            k = c + tol2*a2*prev2
            dm = (b2*prev + np.sqrt(-a2*b2*prev2 + (a2+b2)*k)) / (a2+b2)
            dm_list.append(dm)

        return np.asarray(dm_list, dtype=float)

    dm_list = generate_dm_list(dm_start, dm_end, dt, pulse_width, dm_tol)
    dm_count = dm_list.shape[0]

    # Arguments
    input = np.zeros(nchans * nsamps_padded, dtype=float)
    output = np.zeros(dm_count * nsamps_padded, dtype=float)
    spin_frequencies = np.asarray(
        [ifreq * (1.0 / (nsamps*dt)) for ifreq in range(nfreqs)], dtype=float)

    def get_arguments():
        idm_start = 0
        idm_end = dm_count
        ichan_start = nchans
        in_stride = int(nsamps_padded / 2)
        in_stride = 1
        out_stride = int(nsamps_padded / 2)
        return [np.uint32(nchans),
                np.uint32(nfreqs),
                np.float32(dt),
                spin_frequencies,
                dm_list,
                np.uint32(in_stride),
                np.uint32(out_stride),
                input, output,
                np.uint32(idm_start),
                np.uint32(idm_end),
                np.uint32(ichan_start)]

    def get_problem_size(p):
        ndm_batch_grid = p["NDM_BATCH_GRID"]
        nfreq_batch_grid = p["NFREQ_BATCH_GRID"]
        grid_x = max(int((dm_count + ndm_batch_grid) / ndm_batch_grid), 1)
        grid_y = nfreq_batch_grid
        return (grid_x, grid_y)

    def config_valid(p):
        return p["block_size_x"] == p["NFREQ_BATCH_BLOCK"]

    # setup metrics
    total_gflops = (dm_count * nchans * nfreqs * 13) / 1e9
    metrics = get_metrics(total_gflops)

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(filename, args.overwrite)

    # setup observers
    observer_type = args.observer_type

    # observer default depends on backend
    if observer_type is None:
        if args.backend == "hip":
            observer_type = "pmt"
        else:
            observer_type = "nvml"

    observers = get_observers(observer_type, args.backend, metrics)

    # start tuning
    results, env = kernel_tuner.tune_kernel("dedisperse_kernel", kernel_string,
                                            problem_size=lambda p: get_problem_size(
                                                p),
                                            arguments=get_arguments(),
                                            restrictions=config_valid,
                                            tune_params=tune_params,
                                            verbose=True, metrics=metrics, iterations=4,
                                            grid_div_x=[], grid_div_y=[],
                                            cache=filename_cache,
                                            observers=observers,
                                            lang=args.backend)

    write_output(filename_output, results)
    write_env(filename_env, env)

    return results, env


if __name__ == "__main__":
    parser = get_default_parser()
    parser.add_argument("--name", required=True, help="Device name, used in output filename")
    parser.add_argument("--backend", required=True, choices=["cupy", "hip"], help="Kernel Tuner backend")
    parser.add_argument("--observer", dest="observer_type", required=False, choices=["nvml", "pmt"],
                        help="Kernel Tuner power observer (Default: PMT if backend is HIP, NVML otherwise)")
    args = parser.parse_args()
    results, env = tune(args)
