#!/usr/bin/env python
import kernel_tuner
import numpy as np

from common import *

def tune(args):
    #the device to use
    device = args.device
    device_name = get_device_name(device)

    #the kernel to tune
    kernel_file = "correlator/tensorcore-correlator.cu"
    with open(f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}", 'r') as f:
        kernel_string = f.read()

    filename = f"tensorcore-correlator_{device_name}"
    if (args.suffix):
        filename = f"{filename}_{args.suffix}"

    #kernel parameters
    nr_receivers = 288
    nr_channels = 480
    nr_samples_per_channel = 3072
    nr_polarizations = 2
    nr_baselines = int((nr_receivers * (nr_receivers + 1) / 2))
    nr_receivers_per_block = 64
    nr_bits = 16
    nr_times_per_block = int(128 / nr_bits)

    #setup kernel compile-time constants
    compiler_options = [f"-DNR_RECEIVERS={nr_receivers}",
                        f"-DNR_RECEIVERS_PER_BLOCK={nr_receivers_per_block}",
                        f"-DNR_BITS={nr_bits}",
                        f"-DNR_CHANNELS={nr_channels}",
                        f"-DNR_SAMPLES_PER_CHANNEL={nr_samples_per_channel}",
                        f"-DNR_POLARIZATIONS={nr_polarizations}"]

    #setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [32]
    tune_params["block_size_y"] = [2]
    tune_params["block_size_z"] = [2]
    tune_params["PORTABLE"] = [0, 1]

    #kernel arguments
    input_size = (nr_channels, int(nr_samples_per_channel / nr_times_per_block), nr_receivers, nr_polarizations, nr_times_per_block)
    output_size = (nr_channels, nr_baselines, nr_polarizations, nr_polarizations)
    input_data = np.zeros(np.prod(input_size) * 2).astype(np.int16)
    output_data = np.zeros(np.prod(output_size) * 2).astype(np.int32)
    arguments = [output_data, input_data]

    #setup metrics
    total_flops = 8 * nr_receivers * nr_receivers / 2 * nr_polarizations * nr_polarizations * nr_channels * nr_samples_per_channel
    total_flops = total_flops / 1e9 #gflops
    metrics = get_metrics(total_flops)
    nr_visibilities = nr_samples_per_channel * nr_baselines * nr_channels
    nr_visibilities = nr_visibilities / 1e9 #gvis
    metrics["GVIS/s"] = lambda p: (nr_visibilities) / (p["time"] / 1000.0)

    #setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(filename, args.overwrite)

    #start tuning
    def compute_problem_size():
        blocks_per_dim = int((nr_receivers + nr_receivers_per_block - 1) / nr_receivers_per_block)
        nr_threadblocks_per_channel = int(blocks_per_dim * blocks_per_dim if \
                                      nr_receivers_per_block == 64 else \
                                      blocks_per_dim * (blocks_per_dim + 1) / 2)
        return (nr_threadblocks_per_channel, nr_channels, 1)

    nvmlobserver = get_nvml_observer()
    results, env = kernel_tuner.tune_kernel("correlate", kernel_string,
                    problem_size=compute_problem_size(),
                    arguments=arguments, tune_params=tune_params,
                    verbose=True, metrics=metrics, iterations=32,
                    grid_div_x=[], grid_div_y=[],
                    compiler_options=compiler_options,
                    cache=filename_cache,
                    observers=[nvmlobserver])

    report_most_efficient(results, tune_params, metrics)

    write_output(filename_output, results)
    write_env(filename_env, env)

    return results, env


if __name__ == "__main__":
    parser = get_default_parser()
    args = parser.parse_args()
    results, env = tune(args)
