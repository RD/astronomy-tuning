#!/usr/bin/env python
import kernel_tuner
import numpy as np

from common import *

def tune(args):
    kernel_file = "dedispersion/tdd.cu"
    kernel_path = f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}"
    with open(kernel_path, 'r') as f:
        kernel_string = f.read()

    filename = f"tdd_{args.name}"
    if (args.suffix):
        filename = f"{filename}_{args.suffix}"

    # setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [8, 16, 32]
    tune_params["block_size_y"] = [int(256 / x)
                                   for x in tune_params["block_size_x"]]
    tune_params["block_size_y"] = [8, 16, 32]
    tune_params["IN_NBITS"] = [8]
    tune_params["SAMPS_PER_THREAD"] = [1, 2, 4]
    tune_params["USE_TEXTURE_MEM"] = [0]

    # datatypes
    dedisp_byte = np.uint8
    dedisp_word = np.uint32

    # kernel arguments
    in_nbits = 8
    nchans = 1024
    sampletime_base = 250.0E-6  # Base is 250 microsecond time samples
    downsamp = 1.0
    t_obs = 30.0  # Observation duration in seconds
    dt = downsamp * sampletime_base  # s (0.25 ms sampling)
    nsamps = int(t_obs / dt)
    bits_per_byte = 8
    chan_stride = 1
    dm_stride = 1
    out_nbits = 32
    f0 = 1581.0  # MHz (highest channel)
    bw = 100.0  # MHz
    df = -1.0*bw/nchans  # MHz (This must be negative!)
    dm_start = 2.0   # pc cm^-3
    dm_end = 100.0  # pc cm^-3
    pulse_width = 4.0   # ms
    dm_tol = 1.25
    gulp_size = 655336

    # Initialize DM list
    def generate_dm_list(dm_start, dm_end, dt, ti, tol):
        dt *= 1e6
        f = (f0 + ((nchans/2) - 0.5) * df) * 1e-3
        tol2 = tol*tol
        a = 8.3 * df / (f*f*f)
        a2 = a*a
        b2 = a2 * (nchans*nchans / 16.0)
        c = (dt*dt + ti*ti) * (tol2 - 1.0)

        dm_list = [dm_start]

        while (dm_list[-1] < dm_end):
            prev = dm_list[-1]
            prev2 = prev*prev
            k = c + tol2*a2*prev2
            dm = (b2*prev + np.sqrt(-a2*b2*prev2 + (a2+b2)*k)) / (a2+b2)
            dm_list.append(dm)

        return np.asarray(dm_list, dtype=float)

    dm_list = generate_dm_list(dm_start, dm_end, dt, pulse_width, dm_tol)
    dm_count = dm_list.shape[0]

    # Initialize delay table
    def generate_delay_table():
        delay_table = np.zeros(nchans, dtype=float)
        for c in range(nchans):
            a = 1.0 / (f0+c*df)
            b = 1.0 / f0
            delay_table[c] = 4.15e3/dt * (a*a - b*b)
        return delay_table

    delay_table = generate_delay_table()
    max_delay = int(dm_list[dm_count-1] * delay_table[nchans-1] + 0.5)

    # Compute input size
    nsamps_computed = int(nsamps - max_delay)
    nsamps_computed_gulp_max = min(gulp_size, nsamps_computed)
    nsamps_gulp_max = nsamps_computed_gulp_max + max_delay
    chans_per_word = np.dtype(dedisp_word).itemsize*bits_per_byte / in_nbits
    nchan_words = int(nchans / chans_per_word)
    in_count_gulp_max = nsamps_gulp_max * nchan_words
    in_stride = int(nchans * in_nbits /
                    (np.dtype(dedisp_byte).itemsize * bits_per_byte))
    nsamps_computed_gulp = min(nsamps_computed_gulp_max, nsamps_computed)

    # Compute output size
    out_stride_gulp_samples = nsamps_computed_gulp_max
    out_bytes_per_sample = int(
        out_nbits / (np.dtype(dedisp_byte).itemsize * bits_per_byte))
    out_stride_gulp_bytes = out_stride_gulp_samples * out_bytes_per_sample
    out_count_gulp_max = out_stride_gulp_bytes * dm_count
    out_stride = (nsamps - max_delay) * out_bytes_per_sample

    # Arguments
    input = np.zeros(in_count_gulp_max, dtype=dedisp_word)
    output = np.zeros(out_count_gulp_max, dtype=dedisp_word)

    def get_arguments():
        # Debug
        # print(f"nsamps:         {nsamps}")
        # print(f"in_stride:      {np.uint64(in_stride)}")
        # print(f"dm_count:       {np.uint64(dm_count)}")
        # print(f"dm_stride:      {np.uint64(dm_stride)}")
        # print(f"nchans:         {np.uint64(nchans)}")
        # print(f"chan_stride:    {np.uint64(chan_stride)}")
        # print(f"out_nbits:      {np.uint64(out_nbits)}")
        # print(f"out_stride:     {np.uint64(out_stride)}")

        return [input, np.uint64(in_stride), np.uint64(dm_stride),
                np.uint64(nchans), np.uint64(chan_stride), output,
                np.uint64(out_nbits), np.uint64(out_stride), dm_list]

    def get_compiler_options():
        return [f"-DDM_COUNT={dm_count}",
                f"-DMAX_DELAY={max_delay}",
                f"-DNSAMPS_COMPUTED_GULP={nsamps_computed_gulp}"]

    def get_problem_size(p):
        samps_per_thread = p["SAMPS_PER_THREAD"]
        block_size_x = p["block_size_x"]
        block_size_y = p["block_size_y"]
        nsamp_blocks = int((nsamps - 1) / (samps_per_thread*block_size_x) + 1)
        ndm_blocks = int((dm_count - 1) / block_size_y + 1)
        return (nsamp_blocks, ndm_blocks)

    # setup metrics
    total_gflops = (dm_count * nchans * nsamps_computed_gulp * 2) / 1e9
    metrics = get_metrics(total_gflops)

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(filename, args.overwrite)

    # setup observers
    observer_type = args.observer_type

    # observer default depends on backend
    if observer_type is None:
        if args.backend == "hip":
            observer_type = "pmt"
        else:
            observer_type = "nvml"

    observers = get_observers(observer_type, args.backend, metrics)

    # start tuning
    results, env = kernel_tuner.tune_kernel("dedisperse_kernel", kernel_string,
                                            problem_size=lambda p: get_problem_size(
                                                p),
                                            arguments=get_arguments(),
                                            compiler_options=get_compiler_options(),
                                            tune_params=tune_params,
                                            verbose=True, metrics=metrics, iterations=4,
                                            grid_div_x=[], grid_div_y=[],
                                            cache=filename_cache,
                                            observers=observers,
                                            lang=args.backend)

    write_output(filename_output, results)
    write_env(filename_env, env)

    return results, env


if __name__ == "__main__":
    parser = get_default_parser()
    parser.add_argument("--name", required=True, help="Device name, used in output filename")
    parser.add_argument("--backend", required=True, choices=["cupy", "hip"], help="Kernel Tuner backend")
    parser.add_argument("--observer", dest="observer_type", required=False, choices=["nvml", "pmt"],
                        help="Kernel Tuner power observer (Default: PMT if backend is HIP, NVML otherwise)")
    args = parser.parse_args()
    results, env = tune(args)
