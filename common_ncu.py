from kernel_tuner.observers.ncu import NCUObserver
def get_ncu_observer():
    ncu_metrics = ["dram__bytes.sum",                                       # Counter         byte            # of bytes accessed in DRAM
                   "dram__bytes_read.sum",                                  # Counter         byte            # of bytes read from DRAM
                   "dram__bytes_write.sum",                                 # Counter         byte            # of bytes written to DRAM
                   "smsp__sass_thread_inst_executed_op_fadd_pred_on.sum",   # Counter         inst            # of FADD thread instructions executed where all predicates were true
                   "smsp__sass_thread_inst_executed_op_ffma_pred_on.sum",   # Counter         inst            # of FFMA thread instructions executed where all predicates were true
                   "smsp__sass_thread_inst_executed_op_fmul_pred_on.sum",   # Counter         inst            # of FMUL thread instructions executed where all predicates were true
                   "smsp__sass_thread_inst_executed_op_hadd_pred_on.sum",   # Counter         inst            # of HADD thread instructions executed where all predicates were true
                   "smsp__sass_thread_inst_executed_op_hfma_pred_on.sum",   # Counter         inst            # of HFMA thread instructions executed where all predicates were true
                   "smsp__sass_thread_inst_executed_op_hmul_pred_on.sum",   # Counter         inst            # of HMUL thread instructions executed where all predicates were true
                  ]

    return NCUObserver(metrics=ncu_metrics)

def get_ncu_metrics():
    def total_fp32_flops(p):
        return p["smsp__sass_thread_inst_executed_op_fadd_pred_on.sum"] + 2 * p["smsp__sass_thread_inst_executed_op_ffma_pred_on.sum"] + p["smsp__sass_thread_inst_executed_op_fmul_pred_on.sum"]

    def total_fp16_flops(p):
        return p["smsp__sass_thread_inst_executed_op_hadd_pred_on.sum"] + 2 * p["smsp__sass_thread_inst_executed_op_hfma_pred_on.sum"] + p["smsp__sass_thread_inst_executed_op_hmul_pred_on.sum"]
    
    def total_flops(p):
        return total_fp32_flops(p) + total_fp16_flops(p)

    metrics = dict()
    metrics["read GB/s"] = lambda p: (p["dram__bytes_read.sum"] / 1e9) / (p["time"]/1e3)
    metrics["write GB/s"] = lambda p: (p["dram__bytes_write.sum"] / 1e9) / (p["time"]/1e3)
    metrics["GOP/s"] = lambda p: (total_flops(p) / 1e9) / (p["time"]/1e3)
    metrics["OI"] = lambda p: (total_flops(p)/float(p["dram__bytes.sum"]))
    return metrics