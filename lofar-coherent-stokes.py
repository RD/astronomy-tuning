#!/usr/bin/env python
import itertools
import kernel_tuner
import math
import numpy as np

from common import *


def tune(args):
    # the device to use
    device = args.device
    device_name = get_device_name(device)

    # The kernel to tune
    kernel_string = get_kernel_string(args.file)
    kernel_name = "coherentStokes"
    kernel_source = f"temp_{kernel_name}.cu"
    with open(kernel_source, "w") as f:
        f.write(kernel_string)

    filename = f"lofar-coherent-stokes_{device_name}"
    if args.suffix:
        filename = f"{filename}_{args.suffix}"

    # setup tunable parameters
    tune_params = OrderedDict()
    tune_params["NR_STATIONS"] = [55]
    tune_params["NR_CHANNELS"] = [16]
    tune_params["NR_POLARIZATIONS"] = [2]
    tune_params["NR_TABS"] = [48]
    tune_params["TIME_INTEGRATION_FACTOR"] = [16, 32, 64]
    tune_params["NR_COHERENT_STOKES"] = [1, 4]
    tune_params["COMPLEX_VOLTAGES"] = [0, 1]
    tune_params["NR_SAMPLES_PER_CHANNEL"] = [16, 32, 64, 128, 256, 512]
    tune_params["NR_TIME_REDUCTION_THREADS"] = [1, 64]

    def compute_nr_time_parallel_threads(p):
        result = list(
            itertools.product(p["NR_SAMPLES_PER_CHANNEL"], p["TIME_INTEGRATION_FACTOR"])
        )
        result = [int(a / b) for a, b in result]
        result = [math.gcd(x, 1024) for x in result]
        result = sorted(list(set(result)))
        return result

    tune_params["NR_TIME_PARALLEL_THREADS"] = compute_nr_time_parallel_threads(
        tune_params
    )
    tune_params["OUTPUT_ORDER_SAMPLES_CHANNELS"] = [0, 1]
    tune_params["OUTPUT_ORDER_CHANNELS_SAMPLES"] = [0, 1]

    # setup valid configurations
    def compute_block_dims(p):
        warp_size = 32
        nr_time_parallel_threads = 1
        max_time_parallel_threads = math.gcd(
            int(p["NR_SAMPLES_PER_CHANNEL"] / p["TIME_INTEGRATION_FACTOR"]), 1024
        )
        nr_time_reduction_threads = 1

        # Try to use at least the warpSize number of threads per block
        while (
            nr_time_parallel_threads < warp_size
            and (nr_time_parallel_threads * 2) < max_time_parallel_threads
        ):
            nr_time_parallel_threads *= 2

        # Switch to the reduction mode for sufficiently large time integration factor
        if (
            p["NR_TIME_REDUCTION_THREADS"] > 1
            and p["TIME_INTEGRATION_FACTOR"] >= nr_time_parallel_threads
        ):
            nr_time_parallel_threads = 1
            nr_time_reduction_threads = 64

        return (nr_time_reduction_threads, nr_time_parallel_threads)

    def config_valid(p):
        valid = p["NR_SAMPLES_PER_CHANNEL"] % p["TIME_INTEGRATION_FACTOR"] == 0
        valid &= p["OUTPUT_ORDER_SAMPLES_CHANNELS"] ^ p["OUTPUT_ORDER_CHANNELS_SAMPLES"]
        valid &= p["COMPLEX_VOLTAGES"] == 0 or p["NR_COHERENT_STOKES"] == 4
        block_dim_x, block_dim_y = compute_block_dims(p)
        valid &= p["NR_TIME_REDUCTION_THREADS"] == block_dim_x
        valid &= p["NR_TIME_PARALLEL_THREADS"] == block_dim_y
        return valid

    restrict = config_valid

    # kernel arguments
    max_nr_samples_per_channel = max(tune_params["NR_SAMPLES_PER_CHANNEL"])
    max_nr_stokes = max(tune_params["NR_COHERENT_STOKES"])
    max_time_integration_factor = max(tune_params["TIME_INTEGRATION_FACTOR"])
    max_nr_channels = max(tune_params["NR_CHANNELS"])
    max_nr_polarizations = max(tune_params["NR_POLARIZATIONS"])
    max_nr_tabs = max(tune_params["NR_TABS"])
    input_size = (max_nr_channels, max_nr_samples_per_channel, max_nr_polarizations, max_nr_tabs)
    output_size = (
        max_nr_tabs,
        max_nr_stokes,
        max_nr_samples_per_channel,
        max_time_integration_factor,
        max_nr_channels,
    )
    input_data = np.zeros(np.prod(input_size) * 2).astype(np.complex64)
    output_data = np.zeros(np.prod(output_size)).astype(np.float)
    arguments = [output_data, input_data]

    # setup metrics
    def compute_flops(p):
        return (
            p["NR_CHANNELS"]
            * p["NR_SAMPLES_PER_CHANNEL"]
            * p["NR_TABS"]
            * (
                8
                if p["NR_COHERENT_STOKES"] == 1
                else 20 + 2.0 / p["TIME_INTEGRATION_FACTOR"]
            )
        )

    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: (compute_flops(p) / 1.0e9) / (p["time"] / 1.0e3)
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]
    nvmlobserver = get_nvml_observer()

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(
        filename, args.overwrite
    )

    # start tuning
    def compute_problem_size(p):
        (
            nr_time_reduction_threads,
            nr_time_parallel_threads_per_block,
        ) = compute_block_dims(p)
        grid_dim_x = p["NR_CHANNELS"]
        grid_dim_y = int(
            p["NR_TIME_PARALLEL_THREADS"] / nr_time_parallel_threads_per_block
        )
        grid_dim_z = p["NR_TABS"]
        return (grid_dim_x, grid_dim_y, grid_dim_z)

    results, env = kernel_tuner.tune_kernel(
        kernel_name,
        kernel_string,
        problem_size=lambda p: compute_problem_size(p),
        arguments=arguments,
        tune_params=tune_params,
        restrictions=restrict,
        verbose=True,
        metrics=metrics,
        iterations=1,
        grid_div_x=[],
        grid_div_y=[],
        cache=filename_cache,
        observers=[nvmlobserver],
        block_size_names=["NR_TIME_REDUCTION_THREADS", "NR_TIME_PARALLEL_THREADS"],
    )

    report_most_efficient(results, tune_params, metrics)

    write_output(filename_output, results)
    write_env(filename_env, env)

    return results, env


if __name__ == "__main__":
    parser = get_default_parser()
    parser.add_argument("--file", required=True, help="Path to kernel source file")
    args = parser.parse_args()
    results, env = tune(args)
