// Constant reference for input data
// This value is set according to the constant memory size
// for all NVIDIA GPUs to date, which is 64 KB and sizeof(float) = 4
__constant__ float c_delay_table[16384];

// The number of DMs computed by a single thread block
#ifndef NDM_BATCH_GRID
#define NDM_BATCH_GRID 8
#endif

// The factor with which the loop over observing channels is unrolled
#ifndef NCHAN_BATCH_THREAD
#define NCHAN_BATCH_THREAD 8
#endif

// The spin frequencies are processed in batches of NFREQ_BATCH_GRID thread blocks at once,
// where each thread block processes NFREQ_BATCH_BLOCK spin frequencies per iteration.
#ifndef NFREQ_BATCH_GRID
#define NFREQ_BATCH_GRID 128
#endif
#ifndef NFREQ_BATCH_BLOCK
#define NFREQ_BATCH_BLOCK 256
#endif

// Option to enable/disable caching input samples in shared memory
#ifndef USE_SHARED_MEMORY
#define USE_SHARED_MEMORY 0
#endif

// Option to enable/disable extrapolation of phasor values to
// reduce the number of sine/cosine evaluations.
#ifndef USE_EXTRAPOLATE
#define USE_EXTRAPOLATE 1
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

/*
 * Helper functions
 */
inline __device__ void cmac(float2 &a, float2 b, float2 c)
{
    a.x += b.x * c.x;
    a.y += b.x * c.y;
    a.x -= b.y * c.y;
    a.y += b.y * c.x;
}

inline __device__ float2 cmul(float2 a, float2 b)
{
    return { a.x * b.x - a.y * b.y,
             a.x * b.y + a.y * b.x};
}

extern "C" {
/*
 * dedisperse kernel
 * FDD computes dedispersion as phase rotations in the Fourier domain
 */
__global__ void dedisperse_kernel(
    unsigned int nchan,
    unsigned int nfreq,
    float dt,
    const float *d_spin_frequencies,
    const float *d_dm_list,
    unsigned int in_stride,
    unsigned int out_stride,
    const float2 *d_in,
    float2 *d_out,
    unsigned int idm_start,
    unsigned int idm_end,
    unsigned int ichan_start)
{
    // The DM that the current block processes
    unsigned int idm_current = blockIdx.x;
    // The DM offset is the number of DMs processed by all thread blocks
    unsigned int idm_offset = gridDim.x;
    // The number of DMs to process
    unsigned int ndm = idm_end - idm_start;

    // The first spin frequency that the current block processes
    unsigned int ifreq_start = blockIdx.y * blockDim.x;
    // The spin frequency offset is the number of spin frequencies processed
    // by all thread blocks (in the y-dimension) and threads (in the x-dimension)
    unsigned int ifreq_offset = gridDim.y * blockDim.x;

    // Load DMs
    float dms[NDM_BATCH_GRID];
    for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
    {
        unsigned int idm_idx = idm_current + (i * idm_offset);
        if (idm_idx < ndm)
        {
            dms[i] = idm_idx < idm_end ? d_dm_list[idm_start + idm_idx] : 0.0f;
        }
    }

    // Two input samples (two subsequent spin frequencies, float2 values) are stored as a float4 value
#if USE_SHARED_MEMORY
    __shared__ float2 s_temp[NCHAN_BATCH_THREAD][NFREQ_BATCH_BLOCK + 1][2];
#endif

    for (unsigned int ifreq_current = ifreq_start + threadIdx.x; ifreq_current < nfreq; ifreq_current += ifreq_offset)
    {
        // Load output samples
        float2 sums[NDM_BATCH_GRID];
#pragma unroll
        for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
        {
            unsigned int idm_idx = idm_current + (i * idm_offset);
            if (idm_idx < ndm)
            {
                size_t out_idx = idm_idx * out_stride + ifreq_current;
                sums[i] = d_out[out_idx];
            }
            else
            {
                sums[i] = make_float2(0, 0);
            }
        }

        // Load spin frequency
        float f = 0.0f;
        if (ifreq_current < nfreq)
        {
            f = d_spin_frequencies[ifreq_current];
        }

        // Apply phase rotation to input sample and add to output sample
        for (unsigned int ichan_outer = 0; ichan_outer < nchan; ichan_outer += NCHAN_BATCH_THREAD)
        {
            // Load samples from device memory to shared memory
#if USE_SHARED_MEMORY
            __syncthreads();
            for (unsigned int i = threadIdx.x; i < NCHAN_BATCH_THREAD * (NFREQ_BATCH_BLOCK / 2); i += blockDim.x)
            {
                unsigned int ichan_inner = i / (NFREQ_BATCH_BLOCK / 2);
                unsigned int ifreq_inner = i % (NFREQ_BATCH_BLOCK / 2);
                unsigned int ichan = ichan_outer + ichan_inner;
                size_t in_idx = ichan * in_stride + (ifreq_current - threadIdx.x) + ifreq_inner;
                s_temp[ichan_inner][ifreq_inner][0] = d_in[in_idx + 0];
                s_temp[ichan_inner][ifreq_inner][1] = d_in[in_idx + 1];
            }
            __syncthreads();
#endif

#if USE_EXTRAPOLATE
            // Compute initial and delta phasor values
            float2 phasors[NDM_BATCH_GRID];
            float2 phasors_delta[NDM_BATCH_GRID];
            for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
            {
                float tdm0 = dms[i] * c_delay_table[ichan_start + ichan_outer + 0] * dt;
                float tdm1 = dms[i] * c_delay_table[ichan_start + ichan_outer + 1] * dt;
                float phase0 = 2.0f * ((float)M_PI) * f * tdm0;
                float phase1 = 2.0f * ((float)M_PI) * f * tdm1;
                float phase_delta = phase1 - phase0;
                phasors[i] = make_float2(__cosf(phase0), __sinf(phase0));
                phasors_delta[i] = make_float2(__cosf(phase_delta), __sinf(phase_delta));
            }

#pragma unroll
            for (unsigned int ichan_inner = 0; ichan_inner < NCHAN_BATCH_THREAD; ichan_inner++)
            {
                // Load input sample
#if USE_SHARED_MEMORY
                float2 sample = s_temp[ichan_inner][threadIdx.x / 2][threadIdx.x % 2];
#else
                unsigned int ichan = ichan_outer + ichan_inner;
                float2 sample = d_in[ichan * in_stride + ifreq_current];
#endif

#pragma unroll
                for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
                {
                    // Update sum
                    cmac(sums[i], sample, phasors[i]);

                    // Update phasor
                    phasors[i] = cmul(phasors[i], phasors_delta[i]);
                }
            } // end for ichan_inner
#else
            for (unsigned int ichan_inner = 0; ichan_inner < NCHAN_BATCH_THREAD; ichan_inner++)
            {
                unsigned int ichan = ichan_outer + ichan_inner;

                // Load input sample
#if USE_SHARED_MEMORY
                float2 sample = s_temp[ichan_inner][threadIdx.x / 2][threadIdx.x % 2];
#else
                float2 sample = d_in[ichan * in_stride + ifreq_current];
#endif

#pragma unroll
                for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
                {
                    // Compute DM delay
                    float tdm = dms[i] * c_delay_table[ichan_start + ichan] * dt;

                    // Compute phase
                    float phase = 2.0f * ((float)M_PI) * f * tdm;

                    // Compute phasor
                    float2 phasor = make_float2(__cosf(phase), __sinf(phase));

                    // Update sum
                    cmac(sums[i], sample, phasor);
                }
            } // end for ichan_inner
#endif    // end if USE_EXTRAPOLATE
        } // end for ichan_outer

        // Store result
        if (ifreq_current < nfreq)
        {
#pragma unroll
            for (unsigned int i = 0; i < NDM_BATCH_GRID; i++)
            {
                unsigned int idm_idx = idm_current + i * idm_offset;
                if (idm_idx < ndm)
                {
                    size_t out_idx = idm_idx * out_stride + ifreq_current;
                    d_out[out_idx] = sums[i];
                }
            }
        } // end if ifreq_current < nfreq
    }     // end for ifreq_current loop
} // end dedisperse_kernel
}