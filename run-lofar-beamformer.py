#!/usr/bin/env python
import kernel_tuner
import numpy as np

from common import *
try:
    from common_ncu import *
except:
    pass

def run_tuning(kernel_string, filename_cache):
    # kernel parameters
    nr_stations = 48
    nr_polarizations = 2
    nr_channels = 256
    nr_samples_per_channel = 768
    compute_weights = False
    nr_tabs_per_block = 32
    subband_bandwidth = 200e3 # Hz
    subband_frequency = np.float64(1.5e8) # Hz

    # setup kernel compile-time constants
    compiler_options = [
        f"-DNR_POLARIZATIONS={nr_polarizations}",
        f"-DNR_TIME_PER_PASS={32}",
        f"-DNR_STATIONS_PER_PASS={32}",
        f"-DNR_INPUT_STATIONS={nr_stations}",
        f"-DNR_OUTPUT_STATIONS={nr_stations}",
        f"-DNR_DELAYS={compute_weights * nr_stations}",
        f"-DNR_CHANNELS={nr_channels}",
        f"-DNR_SAMPLES_PER_CHANNEL={nr_samples_per_channel}",
        f"-DSUBBAND_BANDWIDTH={subband_bandwidth}"
    ]

    # setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [nr_polarizations]
    tune_params["block_size_y"] = [nr_tabs_per_block]
    tune_params["NR_TABS"] = list(range(100, 1100, 100))
    tune_params["NR_INPUT_BITS"] = list([16, 32])

    # kernel arguments
    input_size = (
        nr_channels,
        nr_samples_per_channel,
        nr_polarizations,
        nr_stations
    )
    max_nr_tabs = np.max(tune_params["NR_TABS"])
    output_size = (
        nr_channels,
        nr_samples_per_channel,
        nr_polarizations,
        max_nr_tabs
    )
    delays_size = (
        nr_stations,
        max_nr_tabs
    ) # double
    weights_size = (
        max_nr_tabs,
        nr_channels,
        nr_stations
    ) # complex32 or complex64
    input_data = np.zeros(np.prod(input_size)).astype(float)
    output_data = np.zeros(np.prod(output_size)).astype(float)
    station_indices = np.arange(nr_stations)
    delay_indices = np.arange(nr_stations)
    delays_or_weights = np.zeros(max(np.prod(delays_size), np.prod(weights_size))).astype(np.complex64)
    arguments = [output_data, input_data, station_indices, delays_or_weights, delay_indices, subband_frequency]

    # setup metrics
    def compute_flops(p):
        nr_tabs = p["NR_TABS"]
        nr_operations_weights = nr_channels * nr_tabs * nr_polarizations * nr_stations * 3
        nr_operations_cwma = nr_channels * nr_tabs * nr_polarizations * nr_stations * nr_samples_per_channel * 8
        nr_operations = 0
        if compute_weights:
            nr_operations += nr_operations_weights
        nr_operations += nr_operations_cwma
        return nr_operations

    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: (compute_flops(p) / 1.0e9) / (p["time"] / 1.0e3)
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]
    nvmlobserver = get_nvml_observer()
    observers = [nvmlobserver]

    if (args.ncu):
        observers.append(get_ncu_observer())
        metrics.update(get_ncu_metrics())

    # start tuning
    def compute_problem_size(p):
        grid_x = nr_channels
        nr_tabs = p["NR_TABS"]
        grid_y = int((nr_tabs + nr_tabs_per_block - 1) / nr_tabs_per_block)
        return (grid_x, grid_y)

    results, env = kernel_tuner.tune_kernel(
        "beamFormer",
        kernel_string,
        problem_size=lambda p: compute_problem_size(p),
        arguments=arguments,
        tune_params=tune_params,
        verbose=True,
        metrics=metrics,
        iterations=32,
        grid_div_x=[],
        grid_div_y=[],
        compiler_options=compiler_options,
        observers=observers,
        cache=filename_cache,
    )

    return results, env


def tune(args):
    # the device to use
    device = args.device
    device_name = get_device_name(device)

    # the kernel to tune
    kernel_file = f"beamformer/BeamFormer.cu"
    with open(f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}", "r") as f:
        kernel_string = f.read()

    filename = f"beamformer_{device_name}"
    if args.suffix:
        filename = f"{filename}_{args.suffix}"

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(
        filename, args.overwrite
    )

    results, env = run_tuning(kernel_string, filename_cache)

    write_output(filename_output, results)
    write_env(filename_env, env)


if __name__ == "__main__":
    parser = get_default_parser()
    args = parser.parse_args()
    tune(args)
