import argparse
import json
import os
import re

from collections import OrderedDict

import kernel_tuner

try:
    import pycuda.driver as drv
    drv.init()
except:
  pass

# return the result of a/b rounded to the nearest integer value
def ceilDiv(a, b):
    return int((a + b - 1) / b)

# returns the nearest multiple of b that is greater than or equal to a
def roundUp(a, b):
    return int((a + b - 1) / b) * b

def get_device_name(device):
    if drv:
        return drv.Device(device).name().replace(' ', '_')
    else:
        return "unknown"
def get_fallback():
    if os.uname()[1].startswith('node0'):
        return "/cm/shared/package/utils/bin/run-nvidia-smi"
    return "nvidia-smi"

def get_metrics(total_flops):
    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: total_flops / (p["time"] / 1000.0)
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]
    return metrics

def get_default_parser():
    parser = argparse.ArgumentParser(description='Tune kernel')
    parser.add_argument("-d", dest="device", nargs="?", default=0, help="GPU ID to use")
    parser.add_argument("-f", dest="overwrite", action="store_true", help="Overwrite any existing .json files")
    parser.add_argument("-ncu", dest="ncu", action="store_true", help="Enable NCU metrics")
    parser.add_argument("--suffix", help="Suffix to append to output file names")
    return parser

def setup_output_files(filename, overwrite=False):
    filename_cache = f"{filename}_cache.json"
    filename_output = f"{filename}_output.json"
    filename_env = f"{filename}_env.json"

    if (overwrite):
        for filename in [filename_cache, filename_output, filename_env]:
            if os.path.exists(filename):
                os.remove(filename)

    return (filename_cache, filename_output, filename_env)

def write_output(filename_output, results):
    with open(filename_output, 'w') as fh:
        json.dump(results, fh)

def write_env(filename_env, env):
    with open(filename_env, 'w') as fh:
        json.dump(env, fh)

def get_kernel_string(source_path, processed_files=None):
    source_dir = os.path.abspath(os.path.dirname(source_path))
    source_file = os.path.basename(source_path)

    if processed_files is None:
        processed_files = set()

    source_path = os.path.join(source_dir, source_file)

    with open(source_path, 'r') as src_file:
        source_content = src_file.read()

        # Find all included header files
        header_matches = re.findall(r'#include "(.*?)"', source_content)

        for header in header_matches:
            header_path = os.path.join(source_dir, header)

            # Check if the header file exists and hasn't been processed before
            if os.path.exists(header_path) and header_path not in processed_files:
                processed_files.add(header_path)

                # Include the header's content recursively
                header_content = get_kernel_string(f"{source_dir}/{header}", processed_files)

                # Replace the include statement with the header's content
                source_content = source_content.replace(f'#include "{header}"', header_content)

    return source_content

def get_nvml_observer():
    return kernel_tuner.observers.nvml.NVMLObserver(
        [
            "nvml_power",
            "nvml_energy",
            "core_freq",
            "mem_freq",
            "temperature",
        ]
    )

def get_observers(observer_type, backend, metrics=None):
    observers = []

    if observer_type == "pmt":
        from kernel_tuner.observers.pmt import PMTObserver

        if backend == "hip":
            sensor_name = "rocm"
        else:
            sensor_name = "nvidia"
        pmtobserver = PMTObserver({sensor_name: 0}, use_continuous_observer=True)
        observers.append(pmtobserver)

        if metrics:
            metrics["J"] = lambda p: p[f"{sensor_name}_energy"]
            metrics["W"] = lambda p: 1e3 * p[f"{sensor_name}_energy"] / p["time"]
    elif observer_type == "nvml":
        nvmlobserver = get_nvml_observer()
        observers.append(nvmlobserver)

    return observers

def report_most_efficient(results, tune_params, metrics):
    energy_metric = "J"
    if energy_metric in metrics:
        best_config = min(results, key=lambda x: x[energy_metric])
        print("most efficient configuration:")
        kernel_tuner.util.print_config_output(
            tune_params, best_config, quiet=False, metrics=metrics, units=None)
