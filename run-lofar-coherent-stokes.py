#!/usr/bin/env python
import itertools
import kernel_tuner
import numpy as np

from common import *
import coherent_stokes.helper as csh


def run_tuning(kernel_string, filename_cache):
    # kernel parameters
    nr_polarizations = 2
    nr_channels = 1
    nr_samples_per_channel = 196608
    nr_tabs = 128
    nr_coherent_stokes = 4
    complex_voltages = 0
    nr_bandpass_weights = 0
    nr_bits_per_sample = 32

    # setup kernel compile-time constants
    compiler_options = [
        f"-DNR_POLARIZATIONS={nr_polarizations}",
        f"-DNR_CHANNELS={nr_channels}",
        f"-DNR_SAMPLES_PER_CHANNEL={nr_samples_per_channel}",
        f"-DNR_TABS={nr_tabs}",
        f"-DCOMPLEX_VOLTAGES={complex_voltages}",
        f"-DNR_COHERENT_STOKES={nr_coherent_stokes}",
        f"-DOUTPUT_ORDER_SAMPLES_CHANNELS={1}",
        f"-DNR_BANDPASS_WEIGHTS={nr_bandpass_weights}",
        f"-DNR_BITS_PER_SAMPLE={nr_bits_per_sample}"
    ]

    # setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [32]
    tune_params["TIME_INTEGRATION_FACTOR"] = [1, 2, 3] + [2**i for i in range(2, 15)]
    tune_params["NR_TABS_PER_THREAD"] = [1, 8]
    tune_params["USE_WARP_REDUCTION"] = [False, True]

    def config_valid(p):
        nr_tabs_per_thread = p["NR_TABS_PER_THREAD"]
        use_warp_reduction = p["USE_WARP_REDUCTION"]
        time_integration_factor = p["TIME_INTEGRATION_FACTOR"]
        warp_size = 32
        if use_warp_reduction:
            return nr_tabs_per_thread > 1 and time_integration_factor % warp_size == 0
        else:
            return nr_tabs_per_thread == 1 and time_integration_factor < warp_size

    restrict = config_valid

    # kernel arguments
    input_size = (
        nr_channels,
        nr_samples_per_channel,
        nr_polarizations,
        nr_tabs,
    )
    max_nr_samples_per_channel_out = int(
        nr_samples_per_channel / min(tune_params["TIME_INTEGRATION_FACTOR"])
    )
    output_size = (
        nr_tabs,
        nr_coherent_stokes,
        max_nr_samples_per_channel_out,
        nr_channels,
    )
    input_data = np.zeros(np.prod(input_size) * 2).astype(np.complex64)
    output_data = np.zeros(np.prod(output_size)).astype(float)
    arguments = [output_data, input_data]

    # setup metrics
    def compute_flops(p):
        return (
            nr_channels
            * nr_samples_per_channel
            * nr_tabs
            * (
                8
                if nr_coherent_stokes == 1
                else 20 + 2.0 / p["TIME_INTEGRATION_FACTOR"]
            )
        )

    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: (compute_flops(p) / 1.0e9) / (p["time"] / 1.0e3)
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]
    nvmlobserver = get_nvml_observer()

    # start tuning
    def compute_problem_size(p):
        time_integration_factor = p["TIME_INTEGRATION_FACTOR"]
        use_warp_reduction = p["USE_WARP_REDUCTION"]
        if not use_warp_reduction:
            nr_samples_per_block = 32 * time_integration_factor
            grid_x = int((nr_samples_per_channel + nr_samples_per_block - 1) / nr_samples_per_block)
            grid_y = nr_channels
            grid_z = nr_tabs
        else:
            nr_samples_per_block = min(time_integration_factor, 32)
            grid_x = int((nr_samples_per_channel + nr_samples_per_block - 1) / nr_samples_per_block)
            grid_y = nr_channels
            nr_tabs_per_thread = p["NR_TABS_PER_THREAD"]
            grid_z = int((nr_tabs + nr_tabs_per_thread - 1) / nr_tabs_per_thread)
        return (grid_x, grid_y, grid_z)

    results, env = kernel_tuner.tune_kernel(
        "coherentStokes",
        kernel_string,
        problem_size=lambda p: compute_problem_size(p),
        arguments=arguments,
        tune_params=tune_params,
        restrictions=restrict,
        verbose=True,
        metrics=metrics,
        iterations=32,
        grid_div_x=[],
        grid_div_y=[],
        compiler_options=compiler_options,
        observers=[nvmlobserver],
        cache=filename_cache,
    )

    return results, env


def tune(args):
    # the device to use
    device = args.device
    device_name = get_device_name(device)

    # the kernel to tune
    kernel_file = f"coherent_stokes/CoherentStokes.cu"
    with open(f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}", "r") as f:
        kernel_string = f.read()

    filename = f"coherent-stokes_{device_name}"
    if args.suffix:
        filename = f"{filename}_{args.suffix}"

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(
        filename, args.overwrite
    )

    results, env = run_tuning(kernel_string, filename_cache)

    write_output(filename_output, results)
    write_env(filename_env, env)


if __name__ == "__main__":
    parser = get_default_parser()
    args = parser.parse_args()
    tune(args)
