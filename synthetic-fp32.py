#!/usr/bin/env python
import kernel_tuner
import numpy as np

from common import *

def tune(args):
    #the device to use
    device_name = get_device_name(args.device)

    #get device properties
    device = drv.Device(args.device)
    multiprocessor_count = device.get_attribute(drv.device_attribute.MULTIPROCESSOR_COUNT)
    max_block_dim_x = device.get_attribute(drv.device_attribute.MAX_BLOCK_DIM_X)

    #the kernel to tune
    kernel_file = "synthetic/fp32.cu"
    with open(f"{os.path.dirname(os.path.realpath(__file__))}/{kernel_file}", 'r') as f:
        kernel_string = f.read()

    filename = f"synthetic-fp32_{device_name}"
    if (args.suffix):
        filename = f"{filename}_{args.suffix}"

    #setup tunable parameters
    tune_params = OrderedDict()
    tune_params["block_size_x"] = [max_block_dim_x]
    tune_params["nr_outer"] = [64]
    tune_params["nr_inner"] = [1024]

    #kernel arguments
    data_size = (multiprocessor_count, max_block_dim_x)
    data = np.zeros(np.prod(data_size)).astype(float)
    arguments = [data]

    #flop count
    def total_gflops(nr_outer, nr_inner):
        return np.prod(data_size) * nr_outer * nr_inner * 8 / 1e9

    #setup metrics
    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: total_gflops(p["nr_outer"], p["nr_inner"]) / (p["time"] / 1000.0)
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]

    #setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(filename, args.overwrite)

    #start tuning
    nvmlobserver = get_nvml_observer()
    results, env = kernel_tuner.tune_kernel("fp32_kernel", kernel_string,
                    problem_size=(multiprocessor_count),
                    arguments=arguments, tune_params=tune_params,
                    verbose=True, metrics=metrics, iterations=4,
                    grid_div_x=[], grid_div_y=[],
                    cache=filename_cache,
                    observers=[nvmlobserver])

    report_most_efficient(results, tune_params, metrics)

    write_output(filename_output, results)
    write_env(filename_env, env)

    return results, env


if __name__ == "__main__":
    parser = get_default_parser()
    args = parser.parse_args()
    results, env = tune(args)
